<?php

class bd{
    private $dblocation;
    private $dbname;
    private $dbuser;
    private $dbpasswd;
    private $dbcnx;

    function __construct($dblocation, $dbname, $dbuser, $dbpasswd){
        $this->dblocation = $dblocation;
        $this->dbname = $dbname;
        $this->dbuser = $dbuser;
        $this->dbpasswd = $dbpasswd;
    }

    function select_bd(){
        $this->dbcnx = @mysql_connect($this->dblocation,$this->dbuser,$this->dbpasswd) or die(mysql_error());
        mysql_select_db($this->dbname, $this->dbcnx) or die(mysql_error());
    }
}

class FileToArray{
    private $name_file;
    public $logs;

    function __construct($name_file){
        $this->name_file = $name_file;
        $this->logs = array();
    }

    function setArray(){
        $lines = file($this->name_file);
        $ii=0;
        $jj=0;
        foreach($lines as $value){
            $p=0;
            for($i=0;$i<strlen($value);$i++){
                if($value{$i}=="|") {
                    $this->logs[$ii][$jj] = substr($value,$p,$i-$p);
                    $p=$i+1;
                    $jj++;
                }
                if($i == (strlen($value)-1)) {
                    $this->logs[$ii][$jj] = substr($value,$p,strlen($value)-1-$p);
                }
            }
            $ii++;
            $jj=0;
        }
    }
}

class InsertToFile2{
    function SetTable($file2){
        for($i=0;$i<count($file2->logs);$i++){
            $sqll = mysql_query("select MAX(`id`) from file2;");
            $sql2 = mysql_query("select * from file2;");
            $id = mysql_result($sqll,0);
            $k=0;
            while($file = mysql_fetch_array($sql2)){
                if($file[ip] == $file2->logs[$i][0])
                    $k++;
            }
            if($k==0){
                mysql_query("INSERT INTO `file2` VALUES ($id+1,'".$file2->logs[$i][0]."','".$file2->logs[$i][1]."','".$file2->logs[$i][2]."');") or die(mysql_error());
            }
        }
    }
}

class InsertToFile1{
    function SetTable($file1){
        for($i=0;$i<count($file1->logs);$i++){
            $sqll = mysql_query("select MAX(`id`) from file1;");
            $sql2 = mysql_query("select * from file1;");
            $id = mysql_result($sqll,0);
            $datetime=$file1->logs[$i][0]." ".$file1->logs[$i][1];
            $k=0;
            while($file = mysql_fetch_array($sql2)){
                if(($file[datetime] == $datetime) && ($file[ip] == $file1->logs[$i][2]))
                    $k++;
            }
            if($k==0){
                mysql_query("INSERT INTO `file1` VALUES ($id+1,'".$datetime."','".$file1->logs[$i][2]."','".$file1->logs[$i][3]."','".$file1->logs[$i][4]."');") or die(mysql_error());
            }
        }
    }
}

class WtiteTable{
    function write(){
        $sql = mysql_query("SELECT
								f2.ip AS ip,  f2.name_browser AS browser, f2.name_os AS os,
								(SELECT site_last FROM file1 AS f11 WHERE f11.ip=f1.ip ORDER BY f11.datetime ASC LIMIT 1) AS url_last,
								(SELECT site_now FROM file1 AS f11 WHERE f11.ip=f1.ip ORDER BY f11.datetime DESC LIMIT 1) AS url_now,
								COUNT(f1.ip)+1 AS count,
								TimeDiff(MAX( f1.datetime ),MIN( f1.datetime ) ) AS time
						    FROM file2 AS f2 ,file1 AS f1
						    WHERE f1.ip=f2.ip
						    GROUP BY f1.ip");

        echo "<table border=2>";
        echo "<tr><td>IP-адрес</td><td>Браузер</td><td>ОС</td><td>URL с которого зашел в первый раз</td><td>URL с которого зашел в последний раз</td><td>Кол-во просмотренных уникальных URL-адресов</td><td>Общее время пребывания на сайте</td></tr>";
        while($file = mysql_fetch_array($sql)){
            echo "<tr><td>".$file[ip]."</td><td>".$file[browser]."</td><td>".$file[os]."</td><td>".$file[url_last]."</td><td>".$file[url_now]."</td><td>".$file[count]."</td><td>".$file[time]."</td></tr>";
        }
        echo "</table>";

        mysql_close();
    }
}

$select_bd = new bd('localhost', 'Logs', 'root','');
$select_bd->select_bd();

$file1 = new FileToArray('file1.txt');
$file2 = new FileToArray('file2.txt');

$file1->setArray();
$file2->setArray();

$tofile2 = new InsertToFile2();
$tofile2->SetTable($file2);

$tofile1 = new InsertToFile1();
$tofile1->SetTable($file1);

$write = new WtiteTable();
$write->write();

?>